var frisby = require('frisby');

frisby.globalSetup({
	request: {
	inspectOnFailure: true
	}
});

frisby.create('Smoke test: Verify if is possible delete a user')
	.delete('https://todo.ly/api/user/0.json')
	.expectStatus(200)	
	.toss();