var frisby = require('frisby');

frisby.globalSetup({
	request: {
		/*
		proxy: 'http://172.20.240.5:8080/',*/
		headers: {
			Authorization: 'Basic bGVvLmZjeEBnbWFpbC5jb206bGVvIUAjNDU2'
		},
		json: true,
		inspectOnFailure: true
	}
});

frisby.create('Sanity: Verify if an existing user can be updated successfully')
	.put('https://todo.ly/api/user/0.json',
	 {"FullName": "karensita1"})
	.expectStatus(200)	
	.expectJSON({"FullName": "karensita1"})	
	.expectJSONTypes({"FullName":String})
	.toss()