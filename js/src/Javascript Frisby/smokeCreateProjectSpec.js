var frisby = require('frisby');

frisby.globalSetup({
	request: {
		/*
		proxy: 'http://172.20.240.5:8080/',*/
		headers: {
			Authorization: 'Basic bGVvLmZjeEBnbWFpbC5jb206bGVvIUAjNDU2'
		},
		json: true,
		inspectOnFailure: true
	}
});
		
var requestNewProject = {
	"Content": "S",
	"Icon": 4
};

var responseNewProjectTypes = {
			"Content": String,
			"Icon": Number
};

frisby.create('Smoke test: Verify if a valid user can create a project')
	.post('https://todo.ly/api/projects.json', requestNewProject)
		.expectStatus(200)
		.expectJSON(requestNewProject)
		.expectJSONTypes(responseNewProjectTypes)
		.toss();