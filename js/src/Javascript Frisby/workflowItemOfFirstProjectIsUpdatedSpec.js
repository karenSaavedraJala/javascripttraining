var frisby = require('frisby');

frisby.globalSetup({
	request: {		
		headers: {
			Authorization: 'Basic bGVvLmZjeEBnbWFpbC5jb206bGVvIUAjNDU2'
		},
		json: true,
		inspectOnFailure: true
	}
});

frisby.create('Sanity: Getting the first project')
	.get('https://todo.ly/api/projects.json')
		.expectStatus(200)		
		.afterJSON(function(data){
			
			var projectId = data[0].Id;
			
			frisby.create('Create item for the sspecific project')
				.post('https://todo.ly/api/items.json', {
					"Content": "new item1",
					"ProjectId": projectId
				})
					.expectStatus(200)
					.expectJSON({
						"Content": "new item1",
						"ProjectId": projectId
					})
					.afterJSON(function(data){
						//get item id
						var itemId = data.Id;
						//updating item created
						frisby.create('Verify if an item recently created can be Updated')
							.put('https://todo.ly/api/items/' + itemId + '.json', {
								"Content": "Item recently created updated"
							})
								.expectStatus(200)
								.expectJSON({
									"Content": "Item updated"
								})
						.toss();
						
						
						//delete created project to clean environment
						frisby.create('Delete the Item that was created and updated')
							.delete('https://todo.ly/api/projects/' + projectId + '.json')
								.expectStatus(200)								
						.toss();
					})
			.toss();		
		})
.toss()
