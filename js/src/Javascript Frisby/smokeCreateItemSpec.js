var frisby = require('frisby');

frisby.globalSetup({
	request: {
		/*
		proxy: 'http://172.20.240.5:8080/',*/
		headers: {
			Authorization: 'Basic bGVvLmZjeEBnbWFpbC5jb206bGVvIUAjNDU2'
		},
		json: true,
		inspectOnFailure: true
	}
});

frisby.create('smoke: Get all projects for a user')
	.get('https://todo.ly/api/projects.json')
		.expectStatus(200)		
		.afterJSON(function(data){
			
			var projectId = data[0].Id;
			
			frisby.create('Smoke: Verify if a item is created inside a project')
				.post('https://todo.ly/api/items.json', {
					"Content": "My new item",
					"ProjectId": projectId
				})
					.expectStatus(200)
					.expectJSON({
						"Content": "My new item1",
						"ProjectId": projectId
					})				
			.toss();		
		})
.toss()