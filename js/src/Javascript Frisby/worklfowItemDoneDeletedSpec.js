
var frisby = require('frisby');

frisby.globalSetup({
	request: {	
		/*
		proxy: 'http://172.20.240.5:8080/',*/	
		headers: {
			Authorization: 'Basic bGVvLmZjeEBnbWFpbC5jb206bGVvIUAjNDU2'
		},
		json: true,
		inspectOnFailure: true
	}
});

frisby.create('Sanity: Getting the first project')
	.get('https://todo.ly/api/projects.json')
		.expectStatus(200)		
		.afterJSON(function(data){			
			projectId = data[0].Id;
		
			frisby.create('create item1')
			.post('https://todo.ly/api/items.json', {
				"Content": "New item Karensita1",
				"ProjectId": projectId
			})
				.expectStatus(200)
				.expectJSON({
					"Content": "New Karensita1",
					"ProjectId": projectId
				})
				.afterJSON(function(data){					
					itemId = data.Id;
					frisby.create('Sanity: Update the item as done (checheck : True)')
						.put('https://todo.ly/api/items/' + itemId + '.json', {
							"Checked": true
						})
							.expectStatus(200)
							.expectJSON({
								"Checked": true
							})
					.toss();
					
					frisby.create('Deleting done item')
						.delete('https://todo.ly/api/items/' + itemId + '.json')
							.expectStatus(200)
							.inspectJSON()
							.expectJSON({
								"Content": "New Karensita1",
								"Checked": true
							})
					.toss();
					
					//delete created project to clean environment
					frisby.create('Workflow end: Delete done item')
						.delete('https://todo.ly/api/projects/' + projectId + '.json')
							.expectStatus(200)								
					.toss()
				})
			.toss();					
		})
.toss()
