var frisby = require('frisby');

frisby.globalSetup({
	request: {		
		json: true,
		inspectOnFailure: true
	}
});

var request = {
	"Email": "firstOption1@email.com",
    "FullName": "firstOption1",
    "Password": "Control123"
};

var response = {
	"ErrorMessage": "Account with this email address already exists",
	"ErrorCode": 201
};

var responseTypes = {
	"ErrorMessage": String,
	"ErrorCode": Number
};

frisby.create('Sanity test: Verify that is not possible create two users with the same mail address')	
	.post('https://todo.ly/api/user.json', request)
	.expectStatus(200)
	.afterJSON(function(data){
			frisby.create('Sanity test: Verify that is not possible create two users with the same mail address')	
			.post('https://todo.ly/api/user.json', request)
			.expectStatus(200)	
			.expectJSON(response)
			.expectJSONTypes(responseTypes)
			.toss();
	})
	
	.toss();
