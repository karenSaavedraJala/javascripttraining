var frisby = require('frisby');

frisby.globalSetup({
	request: {
		/*
		proxy: 'http://172.20.240.5:8080/',*/
		headers: {
			Authorization: 'Basic bGVvLmZjeEBnbWFpbC5jb206bGVvIUAjNDU2'
		},
		json: true,
		inspectOnFailure: true
	}
});
		
var requestNewProject = {
	"Content": "new for be deleted",
	"Icon": 4
};

var responseNewProjectTypes = {
			"Content": String,
			"Icon": Number
};

frisby.create('Smoke test: Verify if a valid user can create a project')
	.post('https://todo.ly/api/projects.json', requestNewProject)
		.expectStatus(200)
		.expectJSON(requestNewProject)
		.expectJSONTypes(responseNewProjectTypes)
		.afterJSON(function(data){
			var projectId = data.Id;
			frisby.create('Smoke test: Verify if a project can be deleted')
				.delete('https://todo.ly/api/projects/'+ projectId +'.json')
				.expectStatus(200)
				.expectJSON({"Deleted": true})				
				.toss();
				})
		.toss();
		
		