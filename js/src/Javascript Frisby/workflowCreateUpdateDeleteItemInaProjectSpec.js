var frisby = require('frisby');

frisby.globalSetup({
	request: {
		/*
		proxy: 'http://172.20.240.5:8080/',*/
		headers: {
			Authorization: 'Basic bGVvLmZjeEBnbWFpbC5jb206bGVvIUAjNDU2'
		},
		json: true,
		inspectOnFailure: true
	}
});

frisby.create('Get all projects')
	.get('https://todo.ly/api/projects.json')
		.expectStatus(200)		
		.afterJSON(function(data){
			
			var projectId = data[0].Id;
			
			frisby.create('Verify if an item can be Created')
				.post('https://todo.ly/api/items.json', {
					"Content": "My new item",
					"ProjectId": projectId
				})
					.expectStatus(200)
					.expectJSON({
						"Content": "My new item",
						"ProjectId": projectId
					})
					.afterJSON(function(data){
					
						var itemId = data.Id;

						frisby.create('Verify if an item recently created can be Updated')
							.put('https://todo.ly/api/items/' + itemId + '.json', {
								"Content": "Item recently created updated"
							})
								.expectStatus(200)
								.expectJSON({
									"Content": "Item recently created updated"
								})
						.toss();
						
						frisby.create('Delete the Item that was created and updated')
							.delete('https://todo.ly/api/items/' + itemId + '.json')
								.expectStatus(200)
								.inspectJSON()
								.expectJSON({
									"Content": "Item recently created updated"
								})
						.toss();
					})
			.toss();
		
		})
.toss()
