var frisby = require('frisby');

frisby.globalSetup({
	request: {
		/*
		proxy: 'http://172.20.240.5:8080/',*/
		headers: {
			Authorization: 'Basic bGVvLmZjeEBnbWFpbC5jb206bGVvIUAjNDU2'
		},
		json: true,
		inspectOnFailure: true
	}
});


var responseList = ["Inbox","Today","Next","Recycle Bin"];

frisby.create('Sanity: getting all filters')
	.get('https://todo.ly/api/filters.json')
	.expectStatus(200)	
	.afterJSON(function(data){
		for (i = 0; i < 3; i++)	{
			var filterId = data[i].Id;
			frisby.create('Sanity: Verify if a user have the default filters when it is authenticated')
			.get('https://todo.ly/api/filters/'+ filterId +'.json')
			.expectStatus(200)
			.expectJSON({
						"Content": responseList[i]})				
			.toss();
		}		
		})
	.toss()