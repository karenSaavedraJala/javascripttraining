var frisby = require('frisby');

frisby.globalSetup({
	request: {
		/*
		proxy: 'http://172.20.240.5:8080/',*/
		headers: {
			Authorization: 'Basic bGVvLmZjeEBnbWFpbC5jb206bGVvIUAjNDU2'
		},
		json: true,
		inspectOnFailure: true
	}
});

frisby.create('Getting  all projects')
	.get('https://todo.ly/api/projects.json')
		.expectStatus(200)		
		.afterJSON(function(data){
			
			for(i = 0; i < 5; i++){
				var projectId = data[0].Id;

				frisby.create('Verify if an item can be Created')
				.post('https://todo.ly/api/items.json', {
					"Content": "New item to be updated as done",
					"ProjectId": projectId
				})
					.expectStatus(200)
					.expectJSON({
						"Content": "New item to be updated as done",
						"ProjectId": projectId
					})
					.afterJSON(function(data){
					
						var itemId = data.Id;

						frisby.create('Update the item setting it as Done')
							.put('https://todo.ly/api/items/' + itemId + '.json', {
								"Checked": true
							})
								.expectStatus(200)
								.expectJSON({
									"Checked": true
								})
						.toss();
						
						frisby.create('Delete the Item that was created and updated')
							.delete('https://todo.ly/api/items/' + itemId + '.json')
								.expectStatus(200)
								.inspectJSON()
								.expectJSON({
									"Content": "New item to be updated as done",
									"Checked": true
								})
						.toss();
					})
				.toss();
			};
			//.expectJSONLength('Children', 0)
			//.toss();
		})
.toss()
