var frisby = require('frisby');

frisby.globalSetup({
	request: {		
		headers: {
			Authorization: 'Basic bGVvLmZjeEBnbWFpbC5jb206bGVvIUAjNDU2'
		},
		json: true,
		inspectOnFailure: true
	}
});

frisby.create('Sanity: Getting the first project')
	.get('https://todo.ly/api/projects.json')
		.expectStatus(200)		
		.afterJSON(function(data){			
			projectId = data[0].Id;
			
			frisby.create('create item1')
			.post('https://todo.ly/api/items.json', {
				"Content": "New item1",
				"ProjectId": projectId
			})
				.expectStatus(200)
				.expectJSON({
					"Content": "New item1",
					"ProjectId": projectId
				})
				.afterJSON(function(data){					
					itemId1 = data.Id;
					frisby.create('Verify if item of the first project is deleted corretly')
							.delete('https://todo.ly/api/items/' + itemId1 + '.json')
								.expectStatus(200)
								.inspectJSON()
								.expectJSON({
									"Content": "new item1"
								})
					.toss();
				})
			.toss();
					
			frisby.create('create item2')
			.post('https://todo.ly/api/items.json', {
				"Content": "New item2",
				"ProjectId": projectId
			})
				.expectStatus(200)
				.expectJSON({
					"Content": "New item2",
					"ProjectId": projectId
				})
				.afterJSON(function(data){					
					itemId2 = data.Id;
					frisby.create('Verify if item of the first project is deleted corretly')
							.delete('https://todo.ly/api/items/' + itemId2 + '.json')
								.expectStatus(200)
								.inspectJSON()
								.expectJSON({
									"Content": "new item1"
								})
					.toss();
					
					
					frisby.create('Deleting the Item that was created and updated')
						.delete('https://todo.ly/api/projects/' + projectId + '.json')
							.expectStatus(200)								
					.toss()
				})
			.toss();
					
		})
.toss()
