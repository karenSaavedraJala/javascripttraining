var frisby = require('frisby');

frisby.globalSetup({
	request: {	
		headers: {
			Authorization: 'Basic bGVvLmZjeEBnbWFpbC5jb206bGVvIUAjNDU2'
		},	
		json: true,
		inspectOnFailure: true
	}
});

var requestUpdate = {
	"Email": "herbert@email.com"   
};

var response = {
	"ErrorMessage": "Invalid input Data",
	"ErrorCode": 302
};

var responseTypes = {
	"ErrorMessage": String,
	"ErrorCode": Number
};

frisby.create('Sanity test: Error message should be displayed when user tries to update its account with an existent mail address')	
	.put('https://todo.ly/api/user/0.json', requestUpdate)
	.expectStatus(200)
	.expectJSON(response)
	.expectJSONTypes(responseTypes)
	.toss();
