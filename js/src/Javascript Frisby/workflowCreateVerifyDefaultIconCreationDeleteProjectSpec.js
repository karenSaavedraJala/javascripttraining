var frisby = require('frisby');

frisby.globalSetup({
	request: {
		/*
		proxy: 'http://172.20.240.5:8080/',*/
		headers: {
			Authorization: 'Basic bGVvLmZjeEBnbWFpbC5jb206bGVvIUAjNDU2'
		},
		json: true,
		inspectOnFailure: true
	}
});


var responseJsonTypes = {
	"Content": String,
	"Icon": Number
};

var request = {"Content": "My New Project Karen"};

var response = {
	ErrorMessage: "Invalid Id",
	ErrorCode: 301
};

var expectedresponse ={"Content": 'My New Project Karen',
						"Icon": 0}

frisby.create('Get all projects')
	.get('https://todo.ly/api/projects.json')
		.expectStatus(200)
		//.inspectJSON()
		.afterJSON(function(data){
			
			var projectId = data.Id;
		frisby.create('Create a project')
			.post('https://todo.ly/api/projects.json', request)
			.expectStatus(200)	

			.afterJSON(function(data){
				frisby.create('Verify the default icon after project creation')
					.get('https://todo.ly/api/filters/'+ projectId +'.json')
					.expectStatus(200)
					.expectJSON(response)
					//.expectJSONTypes(responseJsonTypes)
					.toss();	
			})

			.toss();
		
		})
	.toss()		



