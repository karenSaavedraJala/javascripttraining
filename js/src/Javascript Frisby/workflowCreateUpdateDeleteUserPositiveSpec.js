var frisby = require('frisby');

frisby.globalSetup({
	request: {		
		json: true,
		inspectOnFailure: true
	}
});

var request = {
	"Email": "AngelinaJolie15@email.com",
    "FullName": "AngieJol15",
    "Password": "Control123"
};

var response = {
	"Email": "AngelinaJolie15@email.com",
    "FullName": "AngieJol15",
    "Password": null
};

var responseTypes = {
	"Email": String,
    "FullName": String,
    "Password": null
};

frisby.create('Create a user')
	.post('https://todo.ly/api/user.json', request)
	.expectStatus(200)
	.expectJSON(response)
	.expectJSONTypes(responseTypes)

	.afterJSON(function(data){
		//var userId = data[0].Id;
		
		frisby.create('Updating the user recently created')
			.put('https://todo.ly/api/user/0.json',
			 {"FullName": "karensita1"})
			.expectStatus(200)	
			.expectJSON({"FullName": "karensita1"})	
			.expectJSONTypes({"FullName":String})			
			.toss();

			frisby.create('Deleting a user that was recently created')
				.delete('https://todo.ly/api/user/0.json')
				.expectStatus(200)	
				.toss();
	})

	.toss();
