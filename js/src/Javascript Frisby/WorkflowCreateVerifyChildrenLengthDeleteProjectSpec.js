var frisby = require('frisby');

frisby.globalSetup({
	request: {
		/*
		proxy: 'http://172.20.240.5:8080/',*/
		headers: {
			Authorization: 'Basic bGVvLmZjeEBnbWFpbC5jb206bGVvIUAjNDU2'
		},
		json: true,
		inspectOnFailure: true
	}
});

var request = {"Content": "My New Project Karen"};

var response = {"Content": "My New Project Karen",
				"Deleted": true };

frisby.create('Create a project')
			.post('https://todo.ly/api/projects.json', request)
			.expectStatus(200)	
			.expectJSON(request)
			.afterJSON(function(data){
			
			var projectId = data.Id;
			frisby.create('Project does not have sub-projects')
				.get('https://todo.ly/api/projects/'+ projectId +'.json')
				
				.expectJSONLength('Children', 0)
				
				.expectJSON({
					Children: function(val){
						expect(val.length).toBe(0);
					}
				})
				
				.afterJSON(function(data){
					expect(data.Children.length).toBe(0);
				})
				
				.after(function(err, res, body){
					console.log(body);					
					expect(body.Children.length).toBe(0);
				})
				
			.toss()

			frisby.create('Project does not have sub-projects')
				.delete('https://todo.ly/api/projects/'+ projectId +'.json')
				.expectStatus(200)	
				.expectJSON(request)       

			})

		.toss()
	