var frisby = require('frisby');

frisby.globalSetup({
	request: {	
		/*
		proxy: 'http://172.20.240.5:8080/',*/	
		headers: {
			Authorization: 'Basic bGVvLmZjeEBnbWFpbC5jb206bGVvIUAjNDU2'
		},
		json: true,
		inspectOnFailure: true
	}
});

frisby.create('Sanity: Getting the first project')
	.get('https://todo.ly/api/projects.json')
		.expectStatus(200)		
		.afterJSON(function(data){
			
		for(i =  0; i < 10; i++){
			projectId = data[0].Id;
			
			frisby.create('create item'+ i)
			.post('https://todo.ly/api/items.json', {
				"Content": "New item",
				"ProjectId": projectId
			})
				.expectStatus(200)
				.expectJSON({
					"Content": "New item1",
					"ProjectId": projectId
				})
			.toss();		
			
					
			//delete created project to clean environment
			frisby.create('Delete the Item that was created and updated')
				.delete('https://todo.ly/api/projects/' + projectId + '.json')
					.expectStatus(200)								
			.toss()
		})
		}
.toss()
