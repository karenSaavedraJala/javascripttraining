var frisby = require('frisby');

frisby.globalSetup({
	request: {		
		json: true,
		inspectOnFailure: true
	}
});

var request = {
	"Email": "AngelinaJolie10@email.com",
    "FullName": "AngieJol10",
    "Password": "Control123"
};

var response = {
	"Email": "AngelinaJolie10@email.com",
    "FullName": "AngieJol10",
    "Password": null
};

var responseTypes = {
	"Email": String,
    "FullName": String,
    "Password": null
};

frisby.create('Smoke test: Verify if is possible create an user with valid data')
	.post('https://todo.ly/api/user.json', request)
	.expectStatus(200)
	.expectJSON(response)
	.expectJSONTypes(responseTypes)
	.toss();
