/* GUESS game this game have the input of a number and based on that a matrix is created
   and filled out with pairs of letters.
   User do their guesses and when all letters are guessed the game ends
*/

/* Main function do get the data from the prompt
*/
//var function() {

	var getMatrixSize = function() {
		
		do {
			var inputData = prompt('Enter the size of the matrix for begin the game (even number)');
			if (inputData != null){
				inputData = parseInt(inputData);
				isNumber(inputData);}
		} while((inputData % 2) != 0);							
		return inputData;	
	};
	
	/* Parameters: 
		input: user input from the prompt
		This function validates if the user input is a number if its not
		an alert message will be arise	
	*/
	var isNumber = function(input){
		if(isNaN(input))
			alert('Please input just numbers, strings are not valid');
		return input;	
	};
	
	/*Parameters:
		rowsAndColumnsNumber: integer number
		This function generates a matrix with the same number of columns and rows
		according to number entered, this matrix is filled out with '*'
	*/
	var generateTmpMatrix = function(rowsAndColumnsNumber) {
		var matrix = [];
		for(var i= 0; i < rowsAndColumnsNumber; i++) {	
			matrix.push([]);
			matrix[i].push( new Array(rowsAndColumnsNumber));
			for(var j= 0; j < rowsAndColumnsNumber; j++) {
				matrix[i][j] = '*';
			}		
		}	
		return matrix;
	};
	
	/* Parameters:
		size: integer number
		This function takes the matrix generated on generateTmpMatrix and fills it
		with pair or letters, located on random position of the matrix
	*/
	var fillMatrixWithLetters = function(size){
	
		var matrix = generateTmpMatrix(size);
		var count = 0;
		//i = for iterate rows
		for(var i = 0; i < size; i++){
			//j for iterate columns
			for(var j = 0; j < size / 2; j++){
			
				var isCellready = false;
				do{
					var idx1 = parseInt(Math.random() * (size));
					var idx2 = parseInt(Math.random() * (size));
					if (matrix[idx1][idx2] == '*'){
						matrix[idx1][idx2] = String.fromCharCode(65 + count);
						isCellready = true;					
					}
				} while(!isCellready);	
				isCellready = false;
				do{
					idx1 = parseInt(Math.random() * (size));
					idx2 = parseInt(Math.random() * (size));
					if (matrix[idx1][idx2] == '*'){
						matrix[idx1][idx2] = String.fromCharCode(65 + count);
						isCellready = true;
						count ++;
					}
				} while(!isCellready)
			}
		}
		return matrix;
	};

	/*calling to the related functions to get the needed things in order to have
	  the game.
	  Variables:
		  size: call to the function tmpMatrix to obtain the initial value for the 
		        matrix generation
		  matrix: call to the function fillMatrixWithLetters , fills the matrix
		          with the letters pairs
		  tmpMatrix: call to the function generateTmpMatrix to generate the matrix with '*'
	*/
	
function main(){	
	var size = getMatrixSize();
	var matrix = fillMatrixWithLetters(size);
	var tmpMatrix = generateTmpMatrix(size);
	
	do {
		console.log('TESTING matrix:', matrix);
		console.log('GAME matrix:', tmpMatrix);
		
		alert('Please input the firts coordinates for the guess game');
		var firstGuessRow = parseInt(prompt('Row'));
		if (!isNumber(firstGuessRow)){
			var firstGuessColumn = parseInt(prompt('Column'));
		}
		alert('Please input the second coordinates for the guess game');
		var secondGuessRow = parseInt(prompt('Row')); 
		if (!isNumber(firstGuessRow)){
			var secondGuessColumn = parseInt(prompt('Column')); 
		}
		if (matrix[firstGuessRow][firstGuessColumn] == matrix[secondGuessRow][secondGuessColumn]) {
			console.log('YES! you have a match int he game');
			alert('You have a match!!');
			
			tmpMatrix[firstGuessRow][firstGuessColumn] = matrix[firstGuessRow][firstGuessColumn];
			tmpMatrix[secondGuessRow][secondGuessColumn] = matrix[firstGuessRow][firstGuessColumn];
			
			alert('So far you have the following accerts' + tmpMatrix);
		}
		else{
			console.log('FAIL, Please try again');
			alert('You Fail, Please try again');
			
			alert('So far you have the following matchs' + tmpMatrix);
		}
	} while(tmpMatrix.indexOf('*') >= 0);
}//();